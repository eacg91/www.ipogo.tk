$(document).ready(main);

var contador = 1;

	function main () {
		$('.menu_bar').click(function(){
			if (contador == 1) {
				$('nav').animate({
					left: '0'
				});
				contador = 0;
			} else {
				contador = 1;
				$('nav').animate({
					left: '-100%'
				});
			}
		});

		// Mostramos y ocultamos submenus
		$('.submenu').click(function(){
			$(this).children('.children').slideToggle();
		});
	}

	function countdown(id,limite){
		var fecha=new Date(limite)
		var hoy=new Date()
		var dias=0
		var horas=0
		var minutos=0
		var segundos=0

		if (fecha>hoy){
			var diferencia=(fecha.getTime()-hoy.getTime())/1000
			dias=Math.floor(diferencia/86400)
			diferencia=diferencia-(86400*dias)
			horas=Math.floor(diferencia/3600)
			diferencia=diferencia-(3600*horas)
			minutos=Math.floor(diferencia/60)
			diferencia=diferencia-(60*minutos)
			segundos=Math.floor(diferencia)

			document.getElementById(id).innerHTML=' '+horas+' : '+minutos+' : '+segundos

			if (dias>0 || horas>0 || minutos>0 || segundos>0){
				setTimeout("countdown(\"" + id + "\",\""+limite+"\")",1000)
			}
		}
		else{
			document.getElementById(id).innerHTML='FINALIZADO'
		}
	}

