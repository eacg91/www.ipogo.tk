<?php
	include("conectar_Usuario.php");
	
	function masMinutos($minutos,$var){
		return date("Y-m-d H:i:s",strtotime("$minutos minutes",strtotime($var)));
	}
	
	$sql = "SELECT * FROM onMapCapture";
	$pokemon = $conectar->query($sql);
	if ($pokemon->num_rows > 0) {
		while($row = $pokemon->fetch_assoc()) {
			$onMap_id[] = $row[id];
			$onMap_type[] = $row[type];
			$onMap_idPokemon[] = $row[idPokemon];
			$onMap_reportTime[] = $row[reportTime];
			$onMap_lat[] = $row[lat];
			$onMap_lon[] = $row[lon];
		}
	}else
		echo "¡ERROR consultando Nidos!<hr>";
	
	$sql = "SELECT idPokemon,nameAmerica FROM Pokemon WHERE legendary=0 ORDER BY nameAmerica";
	$pokemon = $conectar->query($sql);
	if ($pokemon->num_rows > 0) {
		while($row = $pokemon->fetch_assoc()) {
			$idPokemon[] = $row[idPokemon];
			$nameAmerica[] = $row[nameAmerica];
			/*** INI Busca ***/
			for($i=0; $i < sizeof($onMap_idPokemon) ;$i++){
				if($onMap_idPokemon[$i]==$row[idPokemon]){
					$onMap_namePokemon[$i] = $row[nameAmerica];
					break;
				}/*else if( $i == (sizeof($onMap_idPokemon)-1) ){
					$onMap_namePokemon[$i] = "ERROR";
				}*/
			}
			/*** FIN Busca ***/
		}
	}else
		echo "¡ERROR consultando Pokemon!<hr>";
?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo $titulo="Avistamientos de Pokemon Salvajes"; ?></title>
	<?php include("_head.php"); ?>
</head>
<body>
	<div id="map" style="position: absolute;width: 100%; height: 100%; top: 0;left: 0; right: 0;"></div>
	
	<div align="center" style="position: absolute; right: 12%; left: 12%; background-color: rgba(80,175,235,0.7);">
		<?php
			include("_menu.php");
		?>
		<grid-dual style="width:100%;border-color:black; background-color: rgba(255,255,255);">
			<h2><?php echo $titulo;?></h2>
			<p>Arrastra el <pokemon id="nombrePokemon1">icono</pokemon> <img src="img/mapa/goplus.ico.png" height="18" id="img_idPokemon">, selecciona un punto en el mapa o escribe coordenadas.</p>
		</grid-dual>
	</div>

</body>
</html>
	<!-- ----------------------------------------------------------- -->
<script>
	var idPokemon = <?php echo json_encode($idPokemon) ?>;
	var nameAmerica = <?php echo json_encode($nameAmerica) ?>;
	var map,markerP;
	
	function cambiaIcono(){
		var id = document.getElementById("idPokemon").value;
		markerP.setIcon('img/Pokemon/'+id+'.ico.png');
		for (var i = 0; i < idPokemon.length; i++) {
			if(id==idPokemon[i]){
				var nombre = nameAmerica[i];
				break;
			}
		}
		document.getElementById("img_idPokemon").src = 'img/Pokemon/'+id+'.ico.png';
		document.getElementById("nombrePokemon").innerHTML = nombre;
		document.getElementById("nombrePokemon1").innerHTML = nombre;
		document.getElementById("nombrePokemon2").innerHTML = nombre;
		document.getElementById("nombrePokemon3").innerHTML = nombre;
	}
	
	function cambiaIconoPosicion(){
		//marker.setPosition(document.getElementById('latF').value,document.getElementById('lonF').value);
		markerP.setPosition(document.getElementById('latF').value,document.getElementById('lonF').value);
	}
	
	function prueba(action){
		document.getElementById('form_nuevoNido').action = "nidos_DB.php?action="+action;
		document.getElementById('form_nuevoNido').form.submit();
	}
	function eliminar(n){
		prueba("eliminar&id="+n);
	}
	
	function initMap() {
		var formWindow = "";
		formWindow += '<form id="form_nuevoNido" name="frmPokemon" method="post" action="nidos_DB.php?action=registrar" accept-charset="UTF-8">';
		formWindow += '<select name="status" id="status">';
			formWindow += '<option value="nido" > Nido de </option><option value="respawn"> Respawn de </option>';
			//formWindow += '<option value="aparicion"> Aparición de </option>';
		formWindow += '</select>';
		formWindow += '<select name="idPokemon" id="idPokemon" onchange="cambiaIcono()">';
			formWindow += '<option value="">Elige un Pokemon</option>';
		for (var i = 0; i < idPokemon.length; i++) {
			formWindow += '<option value="'+idPokemon[i]+'">'+nameAmerica[i]+' #'+idPokemon[i]+'</option>';
		}
		formWindow += '</select>';
		formWindow += '<br>Lat. <input type="number" name="lat" id="latF" value="20.6777" min="0" max="90" step="0.0001" onchange="cambiaIconoPosicion()" required >';
		formWindow += 'Lng. <input type="number" name="lon" id="lonF" value="-103.3472" min="-179.9999" max="180" step="0.0001" onchange="cambiaIconoPosicion()" required >';
		formWindow += '<button style="width:48%" type="submit" ><b>REGISTRAR</b></button>';
		//formWindow += '<button style="width:48%" onclick="prueba(\'ver\')" ><b>VER</b> <pokemon id="nombrePokemon">icono</pokemon> en el mapa</button>';
		formWindow += '<aux id="aux"></aux></form>';
		
		map = new google.maps.Map(document.getElementById('map'), {
			center: new google.maps.LatLng( 20.67555,-103.34835),
			zoom: 14,
			mapTypeId: 'roadmap'
		});
		setCenterGPS(map);
		
		var infowindowNuevo = new google.maps.InfoWindow({
			content: formWindow,
			maxWidth: 240
		});
		
		markerP = new google.maps.Marker({
			position: new google.maps.LatLng( 20.6612,-103.34835),
			map: map,
			title: 'Pokemon',
			icon: 'img/mapa/goplus.ico.png',
			draggable: true
		});
		
		markerP.addListener('click', function(event) {
			infowindowNuevo.open(map, markerP);
		});
		markerP.addListener('drag',function(event) {
			markerP.title = ''+event.latLng.lat().toFixed(4)+','+event.latLng.lng().toFixed(4);
			infowindowNuevo.open(map, markerP);
			document.getElementById('latF').value = event.latLng.lat().toFixed(4);
			document.getElementById('lonF').value = event.latLng.lng().toFixed(4);
			//marker.setPosition(event.latLng);
			markerP.setPosition(event.latLng);
		});
		map.addListener('click', function(event) {
			markerP.title = ''+event.latLng.lat().toFixed(4)+','+event.latLng.lng().toFixed(4);
			infowindowNuevo.open(map, markerP);
			document.getElementById('latF').value = event.latLng.lat().toFixed(4);
			document.getElementById('lonF').value = event.latLng.lng().toFixed(4);
			//marker.setPosition(event.latLng);
			markerP.setPosition(event.latLng);
		});
<?php
		for($i=0; sizeof($onMap_idPokemon) > 0 && $i < sizeof($onMap_idPokemon);$i++) {
 ?>
			var marker<?php echo $i;?> = new google.maps.Marker({
				position: {lat: <?php echo $onMap_lat[$i];?>, lng: <?php echo $onMap_lon[$i];?>},
				map: map,
				title: '<?php echo "$onMap_type[$i] de $onMap_namePokemon[$i]";?>',
				label: '<?php echo "$onMap_type[$i] de $onMap_namePokemon[$i]";?>',
				icon: 'img/Pokemon/<?php echo $onMap_idPokemon[$i];?>.ico.png'
			});
			marker<?php echo $i;?>.addListener('click', function() {
				  infowindow<?php echo $i;?>.open(map, marker<?php echo $i;?>);
			});
			var infowindow<?php echo $i;?> = new google.maps.InfoWindow({
				content: "<div align='center'><?php
					echo "<form id='frm_infoWindow$i' name='frm_infoWindow$i' method='post' action='' accept-charset='UTF-8'>";
					echo "$onMap_type[$i] de $onMap_namePokemon[$i]";
					echo "<br>( $onMap_lat[$i],$onMap_lon[$i] )";
					echo "<br>reportado el $onMap_reportTime[$i]";
					echo "<br><button type='submit' style='width:48%'>Editar</button>";
					echo "<button onclick='eliminar($onMap_id[$i])' style='width:48%'><b>Eliminar</b></button>";
				?></div>",
				maxWidth: 200
			});
 <?php
		}
 ?>
	}
</script>
	<!-- ----------------------------------------------------------- -->
<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCM6oGvs1Ax22HKo9EfdDWbCfIAAloyy0U&callback=initMap">
</script>
<?php $conectar->close(); ?>
