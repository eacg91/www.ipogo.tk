<?php
		session_start();
		date_default_timezone_set('America/Mexico_City');
		if($titulo=="incursionespokemongo.tk")
		    $subedir="";
		else
		    $subedir="../";
		$dirPokebola = $subedir."img/Honorball_icon-icons.com_67450.png";
?>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="icon" type="image/png" href="<?php echo $dirPokebola;?>" />
	<link rel="stylesheet" href="<? echo $subedir; ?>PoGO-Awesome-master/css/PoGO-Awesome.1.0.0.css" type="text/css">
	<link rel="stylesheet" href="<? echo $subedir; ?>css/estilos.css">
	<link rel="stylesheet" href="<? echo $subedir; ?>css/fonts.css">
	<link rel="stylesheet" href="<? echo $subedir; ?>css/grids.css">
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<script src="<? echo $subedir; ?>js/main.js"></script>
	<script src="<? echo $subedir; ?>js/w3data.js"></script>
	<script src="<? echo $subedir; ?>js/geo.js"></script>
	<!--	INI Mapas de Leafletjs	-->
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" crossorigin=""
		integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="/>
	<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" crossorigin=""
		integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="></script>
	<!--	FIN Mapas de Leafletjs	-->
	<script>
		function nombreImagen(idPokemon,levelEgg) {
			if(idPokemon>0)
				return "img/Pokemon/"+idPokemon+".ico.png";
			else if(idPokemon==0){
				if(levelEgg==1||levelEgg==2)
					return "img/raidEgg_Starter.png";
				if(levelEgg==3||levelEgg==4)
					return "img/raidEgg_Rare.png";
				if(levelEgg==5)
					return "img/raidEgg_Legendary.png";
			}
		}
	</script>
<?php
	function imagen($idPokemon,$levelEgg){
		if($idPokemon>0)
			return "img/Pokemon/$idPokemon.ico.png";
		else if($idPokemon==0){
			if($levelEgg==1||$levelEgg==2)
				return "img/raidEgg_Starter.png";
			if($levelEgg==3||$levelEgg==4)
				return "img/raidEgg_Rare.png";
			if($levelEgg==5)
				return "img/raidEgg_Legendary.png";
		}
	}
?>
