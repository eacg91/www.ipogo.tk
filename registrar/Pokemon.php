<?php
	include("../conectar_Usuario.php");
	$result = $conectar->query("SELECT * FROM Pokemon");
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$nameAmerica[$row[idPokemon]]=$row[nameAmerica];
			$type1[$row[idPokemon]]=$row[type1];
			$type2[$row[idPokemon]]=$row[type2];
			$raidLevel[$row[idPokemon]]=$row[raidLevel];
			$raidCP[$row[idPokemon]]=$row[raidCP];
			$hayShiny[$row[idPokemon]]=$row[shiny];
			$hayAlola[$row[idPokemon]]=$row[alola];
		}
		$query = $conectar -> query ("SELECT type FROM tiposPokemon");
		if ($result->num_rows > 0) {
			while ($valores = mysqli_fetch_array($query)) {
				$tipos[]=$valores[type];
			}
		}
	}else {
		echo "0 resultados";
	}
	$conectar->close();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo $titulo="www.ipogo.tk"; ?></title>
	<?php include("../_head.php"); ?>
</head>
<body>
	<div align="center" style="position: absolute; right: 0%; left: 0%; background-color: rgba(80,175,235,0.7);z-index: 0;">
		<?php
			include("../_menu.php");
		 ?>
		<h2>Completar Pokedex</h2>
		<li>Los Pokemon enmarcados en Rojo no cuentan con información disponible, puedes registrarlo. Cada Pokemon registrado está enmarcado en Azul.</li>
		<li>Puedes dejar vacía la información que no conozcas o que ya no sea aplicable (nivel de Raid, CP, etc.)</li>
		<li>Los Pokemon registrados tal vez cuentan con información incompleta o desactualizada, puedes actualizarlos.</li>
		<button class="button" style="height:50px;width:24%;" onclick="verPokedex(1)"><big><b> Kanto </b></big></button>
		<button class="button" style="height:50px;width:24%;" onclick="verPokedex(2)"><big><b> Johto </b></big></button>
		<button class="button" style="height:50px;width:24%;" onclick="verPokedex(3)"><big><b> Hoenn </b></big></button>
		<button class="button" style="height:50px;width:24%;" onclick="verPokedex(4)"><big><b> Sinnoh </b></big></button>
<?php
		//for($h=0;$h<2;$h++){
			for($i=1;$i<=493;$i++){
				if( $nameAmerica[$i]=="")
					$color="red";
				else
					$color="blue";
				//if( ($h==0 and $color=="red") or ($h==1 and $color=="blue") ){
					echo "<grid-Pokedex id='ver$i' style='border-color:$color; display:none'>";
 ?>
					<table><tr><td align="center">
						<!--<img src="../img/Pokemon/<?php echo "$i"; if($hayShiny[$i]==1){ echo "_shiny"; } ?>.png" onerror="this.src='../img/Pokemon/<?php echo "$i";?>.png';" height="80" width="80">-->
						<?php
							echo "<div class='pogo pokemon-$i'></div>";
							if($hayShiny[$i]==1){
								//echo "<div class='pogo pokemon-$i"."_shiny'></div>";
							}
						?>
						<form name="frm<?php echo $i; ?>" method="post" action="Pokemon_Registrar.php" accept-charset="UTF-8" class="tm-contact-form" >
							<input type="text" name="idPokemon" value="<?php echo $i; ?>" style="border-color:<?php echo "$color";?>" size="2" readOnly>
							<input type="text" name="nombre" value="<?php echo $nameAmerica[$i];?>" placeholder="Nombre" style="border-color:<?php echo $color;?>;width:100%" required >
							<br>
							<select name="type1" style="width:47%">
							<?php
								if( $type1[$i]!="" )
									echo "<option value='$type1[$i]'>$type1[$i]</option>";
								else
									echo '<option value="">Tipo 1</option>';
								for($j=0;$j<count($tipos);$j++) {
									if( $tipos[$j]!=$type1[$i] )
										echo "<option value='$tipos[$j]'>$tipos[$j]</option>";
								}
							?>
							</select>
							<select name="type2" style="width:47%">
							<?php
								if( $type2[$i]!="" ){
									echo "<option value='$type2[$i]'>$type2[$i]</option>";
									echo "<option value=''> - </option>";
								}else
									echo '<option value="">Tipo 2</option>';
								for($j=0;$j<count($tipos);$j++) {
									if( $tipos[$j]!=$type2[$i] OR $tipos[$j]!=$type1[$i] )
										echo "<option value='$tipos[$j]'>$tipos[$j]</option>";
								}
							?>
							</select>
							<br>
							<input type="checkbox" name="hayShiny" value="1" <?php if($hayShiny[$i]==1){ echo "checked"; } ?>/> Shiny
							<input type="checkbox" name="hayAlola" value="1" <?php if($hayAlola[$i]==1){ echo "checked"; } ?>/> Alola
							<br>
							<select name="raidLevel">
							<?php
								for($j=0;$j<=6;$j++) {
									echo "<option value='$j'";
									if($raidLevel[$i]==$j)
										echo "selected>";
									else
										echo ">";
									if($j==0)
										echo "Sin Raid</option>";
									else if($j==6)
										echo "Raid de Pase EX</option>";
									else if($j>0)
										echo "Raid Nivel $j</option>";
								}
							?>
							</select>
							<br>
							<?php
								if($raidLevel[$i]>0){
									/*
							?>
							Raid CP: <input type="number" name="raidCP" value="<?php echo $raidCP[$i];?>" min="0" style="width:50%">
							<br>
							<?php
								*/
								}
							?>
							<button type="submit" style="width:80%">Actualizar</button>
						</form>
					</td></tr>
					</table>
					</grid-Pokedex>
<?php
				//}
			}
		//}
 ?>
	</div>
</body>
</html>
<!--------------------------------------------------------------------->
<script>
	function verPokedex(N){
		<?php for($i=1;$i<=493;$i++) { ?>
			document.getElementById('<?php echo "ver$i"; ?>').style.display="none";
		<?php } ?>
		
		switch(N){
			case 0:
				<?php for($i=1;$i<=386;$i++) { ?>
					document.getElementById('<?php echo "ver$i"; ?>').style.display="block";
				<?php } ?>
					break;
				case 1:
					<?php for($i=1;$i<=151;$i++) { ?>
						document.getElementById('<?php echo "ver$i"; ?>').style.display="block";
					<?php } ?>
					break;
				case 2:
					<?php for($i=152;$i<=251;$i++) { ?>
						document.getElementById('<?php echo "ver$i"; ?>').style.display="block";
					<?php } ?>
					break;
				case 3:
					<?php for($i=252;$i<=386;$i++) { ?>
						document.getElementById('<?php echo "ver$i"; ?>').style.display="block";
					<?php } ?>
					break;
				case 4:/*
					<?php for($i=387;$i<=493;$i++) { ?>
						document.getElementById('<?php echo "ver$i"; ?>').style.display="block";
					<?php } ?>*/
					break;
		}
	}
</script>
