<?php
	include("../conectar_Usuario.php");
	$result = $conectar->query("SELECT * FROM Pokemon");
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$nameAmerica[$row[idPokemon]]=$row[nameAmerica];
			$type1[$row[idPokemon]]=$row[type1];
			$type2[$row[idPokemon]]=$row[type2];
			$raidLevel[$row[idPokemon]]=$row[raidLevel];
			$raidCP[$row[idPokemon]]=$row[raidCP];
			$hayShiny[$row[idPokemon]]=$row[shiny];
			$hayAlola[$row[idPokemon]]=$row[alola];
		}
		$query = $conectar -> query ("SELECT type FROM tiposPokemon");
		if ($result->num_rows > 0) {
			while ($valores = mysqli_fetch_array($query)) {
				$tipos[]=$valores[type];
			}
		}
	}else {
		echo "0 resultados";
	}
	$conectar->close();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo $titulo="ipogo.tk"; ?></title>
	<?php include("../_head.php"); ?>
</head>
<body>
	<div id="map" style="position: absolute;width: 100%; height: 100%; top: 0;left: 0; right: 0;z-index: 0;"></div>
	
	<div align="center" style="position: absolute; right: 12%; left: 12%; background-color: rgba(80,175,235,0.7);z-index: 1;">
		<?php
			include("../_menu.php");
			$frm = 1;
			if(!empty($_POST["userAction"])){
				include("../conectar_Usuario.php");
					$userName = $_POST["username"];
					$userPass = $_POST["userpass"];
				$userAction = $_POST["userAction"];
				if($userAction==="1"){
					$color = $_POST["colorPlayer"];
					$level = $_POST["level"];
					$whatsApp = $_POST["WhatsApp"];
					$registra = "INSERT INTO players(player,pass,level,color,whatsApp) VALUES ('$userName','$userPass','$level','$color','$whatsApp')";
					if ($conectar->query($registra) === TRUE){
						$frm = 0;
						$userAction="2";
		?>
						<p align='center'><font color='green'>Se ha registrado exitosamente!</font></p>
						<p align='center'>Ahora prueba a iniciar sesión</p>
		<?php
					}else
						echo "<p align='center'><font color='red'>No se pudo registrar!</font></p>";
				}
				if($userAction==="2"){
					$login = $conectar->query("SELECT * FROM players WHERE player='$userName'");
					if ( $login->num_rows == 1 ) {
						while($row = $login->fetch_assoc()) {
							if ($userPass == $row[pass]) {
								$frm = 0;
								$color = $row[color];
								$whatsApp = $row[whatsApp];
		?>
								<p align="center"><font color="green">Has ingresado exitosamente!</font><br>Espera por Novedades</p>
								<img src="../img/Pokemon_Go-_icon-icons.com_<?php echo $color;?>.svg" width="125">
								<img src="." width="125" alt="Imagen de Entrenador en PoGo">
		<?php
								/* AQUI voy a poner panel de usuario y control de sesiones */
								echo "<p>$color</p>";
								//header('Location: mipagina.php');
		?>
								<hr>
								<p>Pokedex Personal</p>
								<p>Busca y ofrece para intercambios</p>
								<hr>
								<p>Tu historial de participación en Raids</p>
								<p>Tus próximas incursiones EX</p>
								<hr>
								<p>Próximamente: Alertas de incursiones en tu WhatsApp</p>
								<p>Próximamente: Alertas de Pokemon raros en tu WhatsApp</p>
								<p>Próximamente: Alertas de misiones en tu WhatsApp</p>
								<hr>
								<p>Establecer/Cambiar Alias de Usuario</p>
								<p>Establecer/Cambiar Contraseña</p>
								<p>Establecer/Cambiar Telefono de WhatsApp</p>
								<p>Cambiar Nivel y Color</p>
								<p>Establecer/Cambiar Ciudad de Origen</p>
								<p>Subir tu captura de imagen de Entrenador</p>
								<hr>
		<?php
							}
						}
					}else{
						echo "<p align='center'><font color='red'>Parece que los datos de ingreso son incorrectos!</font></p>";
					}
				}
				$conectar->close();
			}
			if($frm==1){
		?>
			<div align="center">
				<h2>Usuarios</h2>
				<form name="frmUser" method="post" action="" accept-charset="UTF-8">
					<input type="text" name="username" placeholder="Usuario" style="width:49%" id="username" value="<?php echo $userName; ?>">
					<input type="password" name="userpass" placeholder="Constraseña" style="width:49%" id="userpass">
					<br><br>
					<div id="verColores" style="display:none">
						<input type="radio" name="colorPlayer" value="Amarillo" ><img src="../img/Pokemon_Go-_icon-icons.com_Amarillo.svg" width="25">Amarillo
						<input type="radio" name="colorPlayer" value="Azul" ><img src="../img/Pokemon_Go-_icon-icons.com_Azul.svg" width="25"><font color="blue">Azul</font>
						<input type="radio" name="colorPlayer" value="Rojo" ><img src="../img/Pokemon_Go-_icon-icons.com_Rojo.svg" width="25"><font color="red">Rojo</font>
						<input type="tel" name="WhatsApp" placeholder="WhatsApp" style="width:35%">
						<br> Nivel: <input type="number" name="level" min="5" max="41" value="5" style="width:18%" >
						<br><br>
					</div>
					<input type="radio" name="userAction" value="1" onclick="mostrarCapa('verColores')" required>Registrar
					<input type="radio" name="userAction" value="2" onclick="ocultarCapa('verColores')" required>Login
					<br><br>
					<button type="submit" style="width:80%" onclick="cifrar()"><h2>Go!</h2></button>
				</form>
			</div>
		<?php
			}
		?>
	</div>
</body>
</html>
<!--------------------------------------------------------------------->
<script src="sha1.js"></script>
<script>
		function cifrar(){
			var input_pass = document.getElementById("userpass");
			input_pass.value = sha1(input_pass.value);
		}
		function mostrarCapa(id){
			document.getElementById(id).style.display="block";
		}
		function ocultarCapa(id){
			document.getElementById(id).style.display="none";
		}
</script>
