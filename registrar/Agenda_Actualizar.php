<!DOCTYPE html>
<html>
<head>
	<title>Actualizando Agenda</title>
    <?php
		$minutosLimite = 90;
		function masMinutos($minutos,$var){
			return date("Y-m-d H:i:s",strtotime("$minutos minutes",strtotime($var)));
		}
		include("../_head.php");
		$idRaid = $_GET['idRaid'];
		$enHuevo = $_POST["status"];
		include("../conectar_Usuario.php");
		$sql = "SELECT raidTime,idPokemon FROM raids WHERE idRaid=$idRaid";
		$raidDetails = $conectar->query($sql);
		if ($raidDetails->num_rows == 1) {
			while($row = $raidDetails->fetch_assoc()) {
				$raidTime = $row[raidTime];
				$idPokemon = $row[idPokemon];
			}
		}
    ?>
	<meta http-equiv="Refresh" content="1;url=Agenda.php?idRaid=<?php echo $idRaid;?>">
</head>
<body>
<?php
	include("../_menu.php");
	
	if($enHuevo==1){
		$enHuevo = ",idPokemon=0,levelEgg=".$_POST["levelEgg"] ;
		if($idPokemon>0){
			$raidTime = masMinutos($minutosLimite,$raidTime);
			$enHuevo = ",raidTime='$raidTime'$enHuevo";
		}
	}else if($enHuevo==0){
		$enHuevo = ",idPokemon=".$_POST["idPokemonUpdated"] ;
		if($idPokemon==0){
			$raidTime = masMinutos(0-$minutosLimite,$raidTime);
			$enHuevo = ",raidTime='$raidTime'$enHuevo";
		}
	}
	
	$sql = "UPDATE raids SET idGym=".$_POST["gymUpdated"].$enHuevo." WHERE idRaid=$idRaid";
    if ($conectar->query($sql) === TRUE) {
        echo "<p align='center'><font color='green'> Actualizado Satisfactoriamente ! </font></p>";
    } else {
        echo "<p align='center'><font color='red'> Error actualizando registro: " . $conectar->error . " ! </font></p>";
    }
	$conectar->close();
?>
</body>
</html>
