<?php
	$idRaid = $_GET['idRaid'];
	include("../conectar_Usuario.php");
	
	$sql = "SELECT nameAmerica,Pokemon.idPokemon,name,raidTime,gym.idgym,paseEX,lat,lon,levelEgg,raidLevel,linkGrupo FROM Pokemon,gym,raids
	    WHERE raids.idRaid=$idRaid AND raids.idPokemon=Pokemon.idPokemon AND raids.idGym=gym.idgym";
	$raidDetails = $conectar->query($sql);
	if ($raidDetails->num_rows == 1) {
		while($row = $raidDetails->fetch_assoc()) {
			$nombrePokemon = $row[nameAmerica];
			$idPokemon = $row[idPokemon];
			$nombreGimnasio = $row[name];
			$idGym = $row[idgym];
			//$paseEX = $row[paseEX];
			if($idPokemon==150)
				$paseEX=1;
			else
				$paseEX=0;
			$raidTime = $row[raidTime];
			$lat = $row[lat];
			$lon = $row[lon];
			$levelEgg = $row[levelEgg];
			$raidLevel = $row[raidLevel];
			$linkGrupo = (String)$row[linkGrupo];
			$huevoAbierto = false;
			if($levelEgg!=0 && $idPokemon==0){
				$nombrePokemon = "$nombrePokemon nivel $levelEgg";
				$raidLevel = $levelEgg;
				$raidTime = date("Y-m-d H:i:s",strtotime("-45 minutes",strtotime($raidTime)));
				if( date(strtotime($raidTime)) < date(time()) ){
					$nombrePokemon = "$nombrePokemon (Abierto)";
					$huevoAbierto = true;
				}
			}
			$consultar = $conectar->query("SELECT idPokemon FROM Pokemon WHERE raidLevel=$raidLevel ORDER BY nameAmerica");
			$x = $consultar->num_rows;
			if ($x > 0) {
				while($row = $consultar->fetch_assoc()) {
					$idPokemon_from_raidLevel[] = $row[idPokemon];
				}
			}
		}
	}
	
	$sql = "SELECT idAttack,attackTime FROM attacks WHERE idRaid=$idRaid ORDER BY attackTime";
	$attacksTimes = $conectar->query($sql);
	if ($attacksTimes->num_rows > 0) {
		while($row = $attacksTimes->fetch_assoc()) {
			$idAttack[] = $row[idAttack];
			$attackTime[] = $row[attackTime];
		}
	}
	date_default_timezone_set ("America/Mexico_City");
	$date = date("d/M/Y",strtotime($raidTime));
	$fechaHoraActual = $fechaHoraAttack = date("Y-m-d H:i:s",time());
	$iniTiempo = date("Y-m-d H:i:s",strtotime("-45 minutes",strtotime($raidTime)));
	$limiteTiempo = date("Y-m-d H:i:s",strtotime("-2 minutes",strtotime($raidTime)));
	
	$dif = 60 - date("s",strtotime($fechaHoraAttack));
	if($dif!=60)
		$fechaHoraAttack = date("Y-m-d H:i:s",strtotime("+$dif seconds",strtotime($fechaHoraAttack)));
	$dif = 5-(date("i",strtotime($fechaHoraAttack))%5);
	if($dif!=5)
		$fechaHoraAttack = date("Y-m-d H:i:s",strtotime("+$dif minutes",strtotime($fechaHoraAttack)));
	$backTime = $fechaHoraAttack;
?>

<!DOCTYPE html>
<html>
<head>
	<title>
    	<?php   echo "$nombrePokemon en $nombreGimnasio ($lat,$lon) $date"; ?>
	</title>
	<?php include("../_head.php"); ?>
	<style>
		#map {
			height: 500px;
		}
		.up {
			width: 33%;
		}
		@media screen and (max-width:1024px) {
			#map {
				height: 250px;
			}
			.up {
				width: 49%;
			}
		}
	</style>
</head>
<body>
<?php
	include("../_menu.php");
?>
	<div align="center">
	    <h2>Agenda de incursiones para <?php echo $nombrePokemon; ?></h2>
		<h3>en Gym: <?php echo $nombreGimnasio; if($paseEX==1) echo "<img src='../img/paseEX.png' width='25'>";?></h3>
		<p> antes de <?php echo "$raidTime en <a href='http://www.google.com.mx/maps/place/$lat,$lon'>$lat,$lon</a>";?> </p>
<?php
		if($paseEX==1 && $linkGrupo!=""){ //$linkGrupo="#NoTenemosLink";
?>
		<p> Unete al grupo de WhatsApp <a href="<?php echo $linkGrupo;?>"><?php echo $linkGrupo;?></a> para que te coordines con otros jugadores interesados en este Raid</p>
<?php	} else echo "Para este Raid NO se ha reportado que exista un grupo de WhatsApp para coordinar jugadores";	?>
		<br>
		
		<grid-dual style="border-color:black">
			<div id="map"></div>
<?php
			for($i=0,$w=99/$x; $i<$x && $x>0 && $idPokemon==0 ;$i++)
				echo "<img src='../img/Pokemon/$idPokemon_from_raidLevel[$i].png' width='$w%'>";
?>
		</grid-dual>
<?php
		if( strtotime($fechaHoraActual) < strtotime($limiteTiempo) ){
?>
		<grid-dual style="border-color:blue;">
			<p><b>Elige lo que quieres hacer:</b></p>
			<br><input type="radio" name="eligeFormulario" onclick="eligeFormularioF(1)" <?php if(!$huevoAbierto) echo "checked";?>> Ver Lista de Jugadores
			<br><input type="radio" name="eligeFormulario" onclick="eligeFormularioF(2)"> Inscribirme en Lista
			<br><input type="radio" name="eligeFormulario" onclick="eligeFormularioF(3)" <?php if($huevoAbierto) echo "checked";?>> Corregir Datos de Raid
			<br>
		</grid-dual>
<?php	} 	?>
		<grid-dual id="ver1" style="border-color:blue;display:<?php if($huevoAbierto) echo "none"; else echo "block"?>">
<?php
			$sql = "SELECT players.player,flyer,color,suggestedTime FROM players,attacks
				WHERE players.player=attacks.player AND attacks.idRaid=$idRaid";
			$attacksDetails = $conectar->query($sql);
			if ($attacksDetails->num_rows > 0) {
				echo "<h3>Jugadores interesados ($date):</h3>";
				while($row = $attacksDetails->fetch_assoc()) {
					if($row[flyer]==0)
						$flyer="terrestre";
					else
						$flyer="volador";
					echo "<p><img src='../img/Pokemon_Go-_icon-icons.com_$row[color].svg' width='35'>
						$row[player] es $flyer y se compromete a llegar en $row[suggestedTime] <hr></p>";
				}
			} else {
				echo "<h3>NO hay jugadores que hayan confirmado para incursionar aquí ($date).</h3>";
				echo '<br><input type="radio" name="eligeFormulario" onclick="eligeFormularioF(2)"> Inscribirme en Lista';
			}
?>
		</grid-dual>
<?php
		if( strtotime($fechaHoraActual) < strtotime($limiteTiempo) ){	
?>
		<grid-dual id="ver2" style="border-color:blue;display:none">
			<form name="frmIncursionar" method="post" action="Agenda_Registrar.php?idRaid=<?php echo $_GET['idRaid'];?>" accept-charset="UTF-8">
				<br>
				<input type="radio" name="userRegistered" value="1" required checked onclick="ocultarCapa('verColores')"> Soy Usuario
				<input type="radio" name="userRegistered" value="0" required onclick="mostrarCapa('verColores')"> Nuevo Usuario
				<!--<input type="radio" name="userRegistered" value="0" required onclick="mostrarCapa('verColores')"> Actualizar Usuario-->
				<br>
				<input type="text" name="username" placeholder="Usuario" class="up" value="<?php echo $userName; ?>" id="username">
				<!--<input type="password" name="userpass" placeholder="Constraseña" value="" class="up" id="userpass">-->
				<br>
				<div id="verColores" style="display:none">
    				Nivel: <input type="number" name="level" min="5" max="41" value="5" style="width:18%" >
    				<br>
    				<input type="radio" name="colorPlayer" value="Amarillo" ><img src="../img/Pokemon_Go-_icon-icons.com_Amarillo.svg" width="25">Amarillo
    				<input type="radio" name="colorPlayer" value="Azul" ><img src="../img/Pokemon_Go-_icon-icons.com_Azul.svg" width="25"><font color="blue">Azul</font>
    				<input type="radio" name="colorPlayer" value="Rojo" ><img src="../img/Pokemon_Go-_icon-icons.com_Rojo.svg" width="25"><font color="red">Rojo</font>
    				<br><br>
				</div>
				<input type="radio" name="userType" value="0" required checked> Terrestre (legal)
				<input type="radio" name="userType" value="1" required> Volador (con hack)
				<br><br>
				Compromiso de llegada:
				<select name="suggestedTime" required>
<?php
					if( strtotime($fechaHoraActual) >= strtotime($iniTiempo) ){
					echo "<option value='$fechaHoraActual'>".date("H:i:s",strtotime($fechaHoraActual))."</option>";
					}
					while( strtotime($fechaHoraAttack) < strtotime($limiteTiempo) ){
					    if( strtotime($fechaHoraAttack) >= strtotime($iniTiempo) ){
						    echo "<option value='$fechaHoraAttack'>".date("H:i:s",strtotime($fechaHoraAttack))."</option>";
					    }
						$fechaHoraAttack = date("Y-m-d H:i:s",strtotime("+5 minutes",strtotime($fechaHoraAttack)));
					}
					$fechaHoraAttack = $backTime;
					echo "<option value='$limiteTiempo'>".date("H:i:s",strtotime($limiteTiempo))."</option>";
?>
				</select>
				<br><br>
				<button type="submit" style="width:90%" onclick="cifrar()"><p align="center"><big><b>LLegaré a participar en esta incursión, aprox. a la hora prometida. </b></big></p></button>
				<br><br>
			</form>
		</grid-dual>
		
		<grid-dual id="ver3" style="border-color:blue;display:<?php if($huevoAbierto) echo "block"; else echo "none"?>">
		    <h3> Actualizar Información </h3><br>
			<form name="frmChoose" method="post" action="Agenda_Actualizar.php?idRaid=<?php echo $idRaid; ?>" accept-charset="UTF-8">
				<select name="gymUpdated" style="width:99%">';
<?php
					echo "<option value='$idGym'>$nombreGimnasio ( $lat , $lon )</option>";
					//listaGimnasios($conectar,$idGym);
					$gimnasios = $conectar->query("SELECT * FROM gym ORDER BY name");
                	if ($gimnasios->num_rows > 0)
                		while($row = $gimnasios->fetch_assoc()) {
                		    if($row[idgym] != $idGym )
    							echo "<option value='$row[idgym]'>$row[name] ( $row[lat] , $row[lon] )</option>";
                		}
                	else
                		echo "¡ERROR consultando gimnasios!<hr>";
?>
				</select>
				
				<div>
					<input type="radio" name="status" value="0" onclick="alternarCapas(0)" required>Abierto
					<input type="radio" name="status" value="1" onclick="alternarCapas(1)" required>En Huevo
				</div>
				
				<div id="verNivel" style="display:none">
					Nivel <select name="levelEgg">
<?php
					for($i=5;$i>0;$i--)
						echo "<option value='$i' >$i </option>";
?>
					</select>
				</div>
			
				<div id="verPokemon" style="display:none">
					<select name="idPokemonUpdated" style="width:50%">';
<?php
						echo "<option value='$idPokemon' > $nombrePokemon ($idPokemon) </option>";
						$f = true;
						$pokemon = $conectar->query("SELECT idPokemon,nameAmerica,raidLevel FROM Pokemon WHERE raidLevel>0 AND raidLevel<6 ORDER BY raidLevel DESC, nameAmerica");
						if ($pokemon->num_rows > 0)
							while($row = $pokemon->fetch_assoc()) {
								if( $row[raidLevel]==4 && $f && $idPokemon!=150 ){
									echo "<option value='150' >EX - Mew Two (150) </option>";
									$f=false;
								}
								if($row[idPokemon]!=$idPokemon)
									echo "<option value='$row[idPokemon]' >$row[raidLevel] - $row[nameAmerica] ($row[idPokemon]) </option>";
							}
?>
					</select>
				</div>
				
				<button type="submit" style="width:25%" onclick="cifrar()"> <b>Actualizar</b> </button>
			</form>
			
		</grid-dual>
<?php
		}
	$conectar->close();
?>
	</div>
	
</body>
</html>

<script src="sha1.js"></script>
<script>
		/*
		function cifrar(){
			var input_pass = document.getElementById("userpass");
			input_pass.value = sha1(input_pass.value);
		}
		*/
		function mostrarCapa(id){
			document.getElementById(id).style.display="block";
		}
		function ocultarCapa(id){
			document.getElementById(id).style.display="none";
		}
		function alternarCapas(N){
			switch(N){
				case 0:
					mostrarCapa('verPokemon');
					ocultarCapa('verNivel');
					break;
				case 1:
					mostrarCapa('verNivel');
					ocultarCapa('verPokemon');
					break;
			}
		}
		function eligeFormularioF(N){
			switch(N){
				case 1:
					document.getElementById('ver1').style.display="block";
					document.getElementById('ver2').style.display="none";
					document.getElementById('ver3').style.display="none";
					break;
				case 2:
					document.getElementById('ver1').style.display="none";
					document.getElementById('ver2').style.display="block";
					document.getElementById('ver3').style.display="none";
					break;
				case 3:
					document.getElementById('ver1').style.display="none";
					document.getElementById('ver2').style.display="none";
					document.getElementById('ver3').style.display="block";
					break;
			}
		}
</script>
<script>
      var map;
      function initMap() {
         map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(<?php echo "$lat, $lon"; ?>),
            zoom: 16,
            mapTypeId: 'roadmap'
         });
         var marker = new google.maps.Marker({
            position: new google.maps.LatLng(<?php echo "$lat, $lon"; ?>),
            icon: '<?php echo "../".imagen($idPokemon,$levelEgg); ?>',
            title: '<?php echo $nombrePokemon; ?>',
            map: map
         });
<?php
		if($paseEX==1){
 ?>
			var markerEX = new google.maps.Marker({
				position: new google.maps.LatLng(<?php echo "$lat, $lon"; ?>),
				icon: '../img/mapa/paseEX.png',
				title: '<?php echo $nombrePokemon; ?>',
				map: map
			});
			markerEX.addListener('click', function() {
				infowindow.open(map, markerEX);
			});
<?php
		}
 ?>
         marker.addListener('click', function() {
            infowindow.open(map, marker);
         });
         var infowindow = new google.maps.InfoWindow({
            content: "<?php
				echo "<big><b>$nombrePokemon</b></big> en<br><b>$nombreGimnasio</b>";
				if($paseEX==1)
					echo "<img src='../img/paseEX.png' width='25'>";
				echo "<p>($lat,$lon)</p><p id='demo'>countdown</p><p>Finaliza el $raidTime</p>";
            ?>",
            maxWidth: 300
         });
      }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCM6oGvs1Ax22HKo9EfdDWbCfIAAloyy0U&callback=initMap">
</script>

<script>
	// Set the date we're counting down to
	var countDownDate = new Date("<?php echo date($raidTime); ?>").getTime();
	
	var x = setInterval(function() {
		var distance = countDownDate - new Date().getTime();
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
<?php 
		if($raidLevel==6) {
 ?>
			var displaying = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
<?php 
		} else {
 ?>
			var displaying = minutes + "m " + seconds + "s ";
<?php 
		}
 ?>

		// Display the result in the element with id="demo"
		document.getElementById("demo").innerHTML = displaying;

		// If the count down is finished, write some text 
		if (distance < 0) {
			clearInterval(x);
			document.getElementById("demo").innerHTML = "FINALIZADO";
		}
	}, 1000);	// Update the count down every 1 second
</script>
