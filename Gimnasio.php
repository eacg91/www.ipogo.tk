<?php
		include("conectar_Usuario.php");
		//$autoRegistro = $_GET["registrar"];
		$buscar = $_POST["buscar"];
		
		if (!empty($_POST["name"])){
			$nameB = $_POST["name"];
			if (!empty($_POST["lat"]))
				$latB = $_POST["lat"];
			if (!empty($_POST["lon"]))
				$lonB = $_POST["lon"];
			if (!empty($_POST["description"]))
				$descriptionB = $_POST["description"];
			$noExiste = $conectar->query("SELECT * FROM gym WHERE lat=$lat AND lon=$lon");
			if ($noExiste->num_rows == 0) {
				echo "<h3 align='center'>";
				$insertar = "INSERT INTO gym(name,lat,lon,description) VALUES ('$nameB','$latB','$lonB','$descriptionB')";
				if ($conectar->query($insertar) === TRUE)
					echo "Has registrado el gimnasio: $nameB!";
				else
					echo "Error registrando el gimnasio $nameB!: <br>$sql<br>" . $conectar->error;
				echo "</h3>";
			}else
				echo "<br>Al parecer ya existe un gimnasio en esas coordenadas ";
			unset($_POST['name']);
		}
		
		$gimnasios = $conectar->query("SELECT * FROM gym WHERE (name LIKE '%$buscar%') ORDER BY name");
		if ($gimnasios->num_rows > 0) {
			while($row = $gimnasios->fetch_assoc()) {
				$name[] = $row[name];
				$lat[] = $row[lat];
				$lon[] = $row[lon];
				$description[] = $row[description];
				$numeroEX = 0;
				$sqlEX="SELECT idRaid,MAX(raidTime),count(*) FROM raids WHERE levelEgg=6 AND idGym=$row[idgym]";
				$incursionesEX = $conectar->query($sqlEX);
				if ($incursionesEX->num_rows > 0) {
					while($row2 = $incursionesEX->fetch_assoc()) {
						$idRaidEX = $row2[idRaid];
						$lastEX = $row2['MAX(raidTime)'];
						$numeroEX = $row2['count(*)'];
					}
					if( strtotime($lastEX) > date(time()) ){
						$nextEX = $lastEX;
						$sqlEX="SELECT idRaid,raidTime FROM raids WHERE levelEgg=6 AND idGym=$row[idGym] ORDER BY raidTime DESC LIMIT 1,1";
						$incursionesEXlast = $conectar->query($sqlEX);
						if ($incursionesEXlast->num_rows > 0) {
							while($row3 = $incursionesEXlast->fetch_assoc()) {
								$lastEXid = $row3[idRaid];
								$lastEX = $row3[raidTime];
							}
						}else
							$lastEX = 0;
					}else
						$nextEX = 0;
				}
				if($numeroEX>0)
					$paseEXprob[] = 1;
				else
					$paseEXprob[] = 0;
				$raid_lastEXid[] = $lastEXid;
				$raid_nextEXid[] = $idRaidEX;
				$raid_lastEX[] = $lastEX;
				$raid_nextEX[] = $nextEX;
				$raid_histEX[] = $numeroEX;
			}
		}

		$conectar->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $titulo="Gimnasios de Pokemon Go"; ?></title>
	<?php include("_head.php"); ?>
	<style>
		#map {
			height: 440px;
		}
		#mapForm {
			height: 300px;
		}
		p,select,input,button,grid-dual {
			margin-top: 7px;
			margin-bottom: 7px;
		}
	</style>
	<script>
		function fRegistraGym(){
			if(document.getElementById('verRegistraGym').style.display=="none"){
				initMapF();
				document.getElementById('verRegistraGym').style.display="block";
				document.getElementById('btnRegistrar').innerHTML = "<h2>Ocultar Formulario de Registro</h2>";
			}else if(document.getElementById('verRegistraGym').style.display=="block"){
				document.getElementById('verRegistraGym').style.display="none";
				document.getElementById('verBuscaGym').style.display="none";
				document.getElementById('btnBuscar').innerHTML = "<h2>Buscar Gimnasio</h2>";
				document.getElementById('btnRegistrar').innerHTML = "<h2>Registro de Nuevo Gym</h2>";
			}
		}
		function fBuscaGym(){
			if(document.getElementById('verBuscaGym').style.display=="none"){
				document.getElementById('verBuscaGym').style.display="block";
				document.getElementById('btnBuscar').innerHTML = "<h2>Ocultar Busqueda</h2>";
			}else if(document.getElementById('verBuscaGym').style.display=="block"){
				document.getElementById('verRegistraGym').style.display="none";
				document.getElementById('verBuscaGym').style.display="none";
				document.getElementById('btnBuscar').innerHTML = "<h2>Buscar Gimnasio</h2>";
				document.getElementById('btnRegistrar').innerHTML = "<h2>Registro de Nuevo Gym</h2>";
			}
		}
		function getParameterByName(name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
			return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		}
	</script>
</head>
<body>
	<div id="map" style="position: absolute;width: 100%; height: 100%; top: 0;left: 0; right: 0;"></div>
	
	<div align="center" style="position: absolute; right: 12%; left: 12%; background-color: rgba(80,175,235,0.7);">
		<?php
			include("_menu.php");
		?>
		<button id="btnRegistrar" onclick="fRegistraGym()" style="width:49%"><h2>Registro de Nuevo Gym</h2></button>
		<button id="btnBuscar" onclick="fBuscaGym()" style="width:49%"><h2>Buscar Gimnasio</h2></button>
		
		<grid-dual id="verRegistraGym" style="<?php if(empty($_GET["registrar"])){ ?>display:none;<?php } ?>border-color:black;background-color:rgba(255,255,255);">
			<h2>Registrar Nuevo Gimnasio</h2>
			<form name="frmGym" method="post" action="" accept-charset="UTF-8" >
				<input type="text" name="name" placeholder="Nombre de Gimnasio" style="width:95%" required >
				<br>
				<input type="text" name="description" placeholder="Describe la Ubicacion del Gimnasio" style="width:95%" >
				<br>
				Lat. <input type="number" name="lat" id="latF" value="20.6777" min="0" max="90" step="0.0001" onchange="initMapF()" required >
				Lng. <input type="number" name="lon" id="lonF" value="-103.3472" min="-179.9999" max="180" step="0.0001" onchange="initMapF()" required >
				<br>
				<button type="submit" style="width:80%">Capturar</button>
			</form>
			<grid-dual style="border-color:black; width:100%">
				<div id="mapForm"></div>
			</grid-dual>
		</grid-dual>
		
		<grid-dual id="verBuscaGym" style="display:none; border-color:black; background-color: rgba(255,255,255);">
			<p><big><b>Antes de registrar un nuevo gimnasio, escribe su nombre para verificar si ya está previamente registrado</b></big></p>
			<form method="post" action="" enctype="multipart/form-data" name="filtrar">
				<input type="text" value="<?php echo $buscar; ?>" name="buscar" placeholder="Buscar" style="border-color:gray;width:75%" >
				<br><button type="submit" name="submit"><h3>Buscar</h3></button>
			</form>
		</grid-dual>
	</div>
	<!-- ----------------------------------------------------------- -->
	<script>
/**********************************************************************/
		function initMapF() {
			var mapF = new google.maps.Map(document.getElementById('mapForm'), {
			  center: new google.maps.LatLng(document.getElementById('latF').value,document.getElementById('lonF').value),
			  zoom: 14,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			
			var markerF = new google.maps.Marker({
			  position: new google.maps.LatLng(document.getElementById('latF').value,document.getElementById('lonF').value),
			  map: mapF,
			  title: 'Ejemplo marcador arrastrable! ',
			  icon: 'img/mapa/gym.png',
			  draggable: true
			});
			
			markerF.addListener('drag',function(event) {
				markerF.title = ''+event.latLng.lat()+','+event.latLng.lng();
				document.getElementById('latF').value = event.latLng.lat().toFixed(4);
				document.getElementById('lonF').value = event.latLng.lng().toFixed(4);
			});
			mapF.addListener('click', function(event) {
				var coordenadas = event.latLng;
				var lat = coordenadas.lat();
				var lng = coordenadas.lng();
				document.getElementById('latF').value = lat.toFixed(4);
				document.getElementById('lonF').value = lng.toFixed(4);
				markerF.setPosition(event.latLng);
			});
		}
/**********************************************************************/
/**********************************************************************/
		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {
			  center: new google.maps.LatLng(20.6745, -103.3555),
			  zoom: 14,
			  mapTypeId: 'roadmap'
			});
<?php
			for($i=0; $gimnasios->num_rows > 0 && $i<sizeof($name);$i++) {
				if($_GET['listaEX']!="1")
					$mostrarGym=1;
				else if($_GET['listaEX']=="1" AND $paseEXprob[$i]==1)
					$mostrarGym=1;
				else
					$mostrarGym=0;
				if($mostrarGym==1){
 ?>
				var marker<?php echo $i;?> = new google.maps.Marker({
				  position: new google.maps.LatLng(<?php echo "$lat[$i],$lon[$i]";?>),
				  map: map,
				  title: '<?php echo "$name[$i] - ( $lat[$i],$lon[$i] )"; 
						if($paseEXprob[$i]==1){ echo " tiene probabilidad de pase EX"; }?>',
				  icon: 'img/mapa/gym<?php if($paseEXprob[$i]==1){ echo "EX"; }?>.png'
				});
				marker<?php echo $i;?>.addListener('click', function() {
				  infowindow<?php echo $i;?>.open(map, marker<?php echo $i;?>);
				  //map.setZoom(15);
				  //map.setCenter(marker<?php echo $i;?>.getPosition());
				});
				var infowindow<?php echo $i;?> = new google.maps.InfoWindow({
				  content: '<div align="center"><?php 
					echo "<b>$name[$i]</b>";
					if($paseEXprob[$i]==1)
						echo '<img src="img/mapa/paseEX.png" width="25">';
					echo "<br>en ( $lat[$i],$lon[$i] ) <p>$description[$i]</p>";
					//echo "<br><a href='https://www.google.com.mx/maps/place/$lat[$i],$lon[$i]'>Ver en Google Maps</a>";
					if($paseEXprob[$i]==1){
						echo "<hr>";
						if($raid_lastEX[$i]!=0)
							echo "Ultimo Pase EX: <a href=registrar/Agenda.php?idRaid=$raid_lastEXid[$i]> $raid_lastEX[$i]</a><br>";
						if($raid_nextEX[$i]!=0)
							echo "Siguiente Pase EX: <a href=registrar/Agenda.php?idRaid=$raid_nextEXid[$i]> $raid_nextEX[$i]</a><br>";
						echo "Incursiones EX registradas: $raid_histEX[$i]";
						//echo "Ultimo Pase EX: <a href=registrar/Agenda.php?idRaid=$raid_idRaidEX[$i]>$raid_lastEX[$i]</a>";
					}
					?> </div>',
				  maxWidth: 220
				});
<?php
				}
			}
 ?>
	/**********************************************************************/
			// Inicializando el mapa cuando se carga la página
			google.maps.event.addDomListener(window, 'load', initialize);
		}
    </script>
    <script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCM6oGvs1Ax22HKo9EfdDWbCfIAAloyy0U&callback=initMap">
    </script>
	<!-- ----------------------------------------------------------- -->
</body>
</html>
